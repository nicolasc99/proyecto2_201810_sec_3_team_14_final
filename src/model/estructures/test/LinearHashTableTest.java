package model.estructures.test;

import junit.framework.TestCase;
import model.data_structures.LinearProbing;
import model.data_structures.List;
import model.data_structures.SeparateChaining;
import model.vo.Servicio;

public class LinearHashTableTest extends TestCase
{
	private LinearProbing<Integer, String> tabladeHash;
	
	
	public void setupEscenario1()
	{
		tabladeHash = new LinearProbing<>();
		
	}
	
	
	public void testAdd()
	{
		setupEscenario1();

		tabladeHash.put(1, "Hola");
		tabladeHash.put(2, "HolaDos");
		tabladeHash.put(3, "HolaTres");
		tabladeHash.put(3, "Sobre_escribo");

		assertEquals(tabladeHash.size(), 3);

	}
	
	
	
	public void testRemove()
	{
		testAdd();
		
		tabladeHash.delete(1);
		tabladeHash.delete(2);
		assertEquals(tabladeHash.size(), 1);

		
		
	}
	
	
	
	public void testGetFirst()
	{
		testAdd();
		
		assertNotSame(tabladeHash.get(1), "xD");
		
		
	}
	
	public void testContiene()
	{

		testAdd();
		
		assertEquals(true, tabladeHash.contains(2));
		
		
		
	}

	
}