package model.estructures.test;

import java.util.Iterator;

import junit.framework.TestCase;
import model.data_structures.LinkedList;
import model.data_structures.Node;
import model.data_structures.Queue;

public class QueueTest extends TestCase {



	private Queue cola;

	private int numeroElementos;


	/**
	 * Construye una nueva lista vacia de Integers
	 */
	public void setupEscenario1( )
	{
		cola = new Queue( );
		numeroElementos = 100;

	}

	/**
	 * Construye una nueva lista con 500 elementos inicialados con el valor de la posicion por 2
	 */
	public void setupEscenario2( )
	{
		numeroElementos = 10;
		cola = new Queue( );


		for( int cont = 0; cont < numeroElementos; cont++ )
		{
			cola.enqueue(( new Integer( cont* 2 ) ));
		}
		

	}

	public void setupEscenario3()
	{
		cola= new Queue();
	}



	/**
	 * Prueba que los elementos se est�n ingresando correctamente a partir de la cabeza
	 */
	public void testA�adir( )
	{
		setupEscenario1( );

		for( int cont = 0; cont < numeroElementos; cont++ )
		{
			cola.enqueue( new Integer( 5 * cont ) );

		}
		// Verificar que la lista tenga la longitud correcta
		assertEquals( "No se adicionaron todos los elementos", numeroElementos, cola.size( ) );


	}
	
	

	public void testEliminar()
	{
		setupEscenario3();
		cola.dequeue();

		assertEquals( "No debia de haber eliminado", 0, cola.size());
	}

	public void testEliminar2()
	{
		setupEscenario2();
		for(int i=0 ; i<numeroElementos; i++)
		{
		cola.dequeue();	
		}

		assertEquals( "No elimino todos los elementos", 0, cola.size());
	}

}
