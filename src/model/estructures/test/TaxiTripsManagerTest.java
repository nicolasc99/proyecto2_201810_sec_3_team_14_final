package model.estructures.test;

import junit.framework.TestCase;
import model.data_structures.List;
import model.data_structures.SeparateChaining;
import model.logic.TaxiTripsManager;
import model.vo.Servicio;

public class TaxiTripsManagerTest extends TestCase {

	private TaxiTripsManager manager= new TaxiTripsManager();
	private SeparateChaining <Double, List<Servicio>> serviciosMillas;
	




	public void setupEscenario1()
	{
		Servicio ser1 = new Servicio("a", "A", 1, 0.2,"A", 32, 2.2);
		Servicio ser2 = new Servicio("b", "B", 1, 1.5,"A", 32, 2.2);
		Servicio ser3 = new Servicio("c", "C", 1, 2.2,"A", 32, 2.2);
		Servicio ser4 = new Servicio("d", "D", 1, 0.4,"A", 32, 2.2);
		Servicio ser5 = new Servicio("e", "E", 1, 3.2,"A", 32, 2.2);
		Servicio ser6 = new Servicio("f", "F", 1, 4.2,"A", 32, 2.2);
		Servicio ser7 = new Servicio("g", "G", 1, 4.5,"A", 32, 2.2);
		Servicio ser8 = new Servicio("h", "H", 1, 2.5,"A", 32, 2.2);
		Servicio ser9 = new Servicio("i", "I", 1, 0.5,"A", 32, 2.2);
		Servicio ser10 = new Servicio("j", "J", 1, 1.2,"A", 32, 2.2);
		serviciosMillas= new SeparateChaining<>();
		
		
	}
	
	public void testServiciosMillas()
	{
		setupEscenario1();
		List<Servicio> lista = new List<Servicio>();
		lista= manager.servicesMiles(0);
		assertEquals("No a�adio todos los servicios", 3, lista.size());
		lista=manager.servicesMiles(1);
		assertEquals("No a�adio todos los servicios", 2, lista.size());
		lista=manager.servicesMiles(2);
		assertEquals("No a�adio todos los servicios", 2, lista.size());
		lista=manager.servicesMiles(3);
		assertEquals("No a�adio todos los servicios", 1, lista.size());
		lista=manager.servicesMiles(4);
		assertEquals("No a�adio todos los servicios", 3, lista.size());
		
	}
	
	
}


