package model.estructures.test;

import java.util.Comparator;
import java.util.Iterator;

import junit.framework.TestCase;
import model.data_structures.Queue;
import model.data_structures.RedBlackTree;

public class RedBlackThreeTest extends TestCase {
	
	private RedBlackTree<Integer,String> arbol= new RedBlackTree<>();
	
	private class IntComparator implements Comparator<Integer>
	{

		@Override
		public int compare(Integer o1, Integer o2) 
		{
			return o1-o2;
		}
		
	}
	
	public Comparator<Integer> intComp()
	{
		return new IntComparator();
	}
	
	public void setupEscenario1()
	{
		Comparator<Integer> c = intComp();
		arbol.put(2, "B",c);
	}
	
	public void setupEscenario2()
	{
		Comparator<Integer> c = intComp();
		arbol.put(2, "B",c);
		arbol.put(1, "A",c);
	}
	
	public void setupEscenario3()
	{
		Comparator<Integer> c = intComp();
		arbol.put(2, "B",c);
		arbol.put(3, "C", c);
	}
	
	public void setupEscenario4()
	{
		Comparator<Integer> c = intComp();
		arbol.put(2, "B",c);
		arbol.put(3, "C", c);
		arbol.put(1, "A",c);
	}
	
	public void setupEscenario5()
	{
		Comparator<Integer> c = intComp();
		arbol.put(5, "E", c);
		arbol.put(1, "A",c);
		arbol.put(20, "R", c);
		arbol.put(3, "C", c);
		arbol.put(8, "H", c);
		arbol.put(21, "X", c);
		arbol.put(15, "M", c);
		arbol.put(18, "P", c);
		arbol.put(13, "L", c);
		
	}
	
	
	public void testPutRaiz()
	{
		int n=2;
		
		assertEquals("El nodo debia ser nulo ", null , arbol.getRoot());
		setupEscenario1();
		assertEquals("La raiz debe ser B ", "B" , arbol.getRoot().getValue()); 
		assertFalse("La raiz debe ser negra ", arbol.isRed(arbol.getRoot()));
		

	}
	
	public void testPutLeft()
	{
		setupEscenario2();
		assertEquals("La raiz debe ser B ", "B" , arbol.getRoot().getValue());
		assertFalse("La raiz debe ser negra ", arbol.isRed(arbol.getRoot()));
		assertTrue("El enlace de A debe ser rojo  ", arbol.isRed(arbol.getRoot().getLeft()));
		assertFalse("El enlace a derecha tiene que ser negro   ", arbol.isRed(arbol.getRoot().getRight()));
		assertEquals("El nodo debia ser nulo ", null , arbol.getRoot().getRight());
	}
	
	public void testPutRight()
	{
		setupEscenario3();
		assertEquals("La raiz debe ser c ", "C" , arbol.getRoot().getValue());
		assertFalse("La raiz debe ser negra ", arbol.isRed(arbol.getRoot()));
		assertEquals("El enlace de la derecha debe ser B ", "B" , arbol.getRoot().getLeft().getValue());
		assertTrue("El enlace de B debe ser rojo  ", arbol.isRed(arbol.getRoot().getLeft()));
		assertFalse("El enlace a derecha tiene que ser negro   ", arbol.isRed(arbol.getRoot().getRight()));
		assertEquals("El nodo debia ser nulo ", null , arbol.getRoot().getRight());
		
	}
	
	public void testPutLeftRight()
	{
		setupEscenario4();
		assertEquals("La raiz debe ser B ", "B" , arbol.getRoot().getValue());
		assertFalse("La raiz debe ser negra ", arbol.isRed(arbol.getRoot()));
		assertFalse("El enlace de A debe ser negro  ", arbol.isRed(arbol.getRoot().getLeft()));
		assertEquals("El enlace de la derecha debe ser A ", "A" , arbol.getRoot().getLeft().getValue());
		assertFalse("El enlace de B debe ser negro  ", arbol.isRed(arbol.getRoot().getLeft()));
		assertEquals("El enlace de la derecha debe ser C ", "C" , arbol.getRoot().getRight().getValue());
		assertFalse("El enlace a derecha tiene que ser negro   ", arbol.isRed(arbol.getRoot().getRight()));
	}
	
	public void testPut()
	{
		setupEscenario5();
		assertEquals("La raiz debe ser M ", "M" , arbol.getRoot().getValue());
		System.out.println(arbol.getRoot().getLeft().getLeft().getValue());
		Queue<Integer> cola;
		try {
			cola = (Queue<Integer>) arbol.keys(arbol.min(), arbol.max());
			System.out.println(cola + " keys");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
//		
//		
	}
//	
	public void testContanins()
	{
		setupEscenario5();
		Comparator<Integer> c = new Icomparator();
		assertTrue("El arbol deberia contener la llave",arbol.contains(18, c));
	}
	
	public void testGet()
	{
		setupEscenario5();
		Comparator<Integer> c= new Icomparator();
		

		
		assertEquals("El valor retornado debia ser x", "X", arbol.get(21, c));
		
		
	}
	
	
	public class Icomparator implements Comparator<Integer>
	{
		@Override
		public int compare(Integer arg0, Integer arg1) {
			
			return arg0-arg1;
		}
	}


	
	
		

}
