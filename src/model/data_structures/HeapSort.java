package model.data_structures;

public class HeapSort <T extends Comparable <T> >{
	
		

	    public void sort(T arr[], boolean condicion)
	    {
	        int n = arr.length;
	 
	        // Construye inicialmente el �rbol.
	        
	        if(condicion)
	        {
	        for (int i = n / 2 - 1; i >= 0; i--)
	            heapify(arr, n, i);
	 
	        // Extrae los elementos del �rbol.
	        for (int i=n-1; i>=0; i--)
	        {
	            // Mueve la ra�z actual al final.
	            T temp = arr[0];
	            arr[0] = arr[i];
	            arr[i] = temp;
	 
	            // Aplica recursividad.
	            heapify(arr, i, 0);
	        }
	        
	        }
	        
	        else
	        {
	        	 
		        for (int i = n / 2 - 1; i >= 0; i--)
		            heapifyI(arr, n, i);
		 
		        // Extrae los elementos del �rbol.
		        for (int i=n-1; i>=0; i--)
		        {
		            // Mueve la ra�z actual al final.
		            T temp = arr[0];
		            arr[0] = arr[i];
		            arr[i] = temp;
		 
		            // Aplica recursividad.
		            heapifyI(arr, i, 0);
		        }
	        	
	        	
	        }
	    }
	    
	    void heapify(T arr[], int n, int raiz)
	    {
	        int largest = raiz;  // Inicializa el mayor como la ra�z.
	        int l = 2*raiz + 1;  // left = 2*i + 1
	        int r = 2*raiz + 2;  // right = 2*i + 2
	 
	        // Si el hijo de la izquierda es mayor a la ra�z.
	        if (l < n && arr[l].compareTo(arr[largest])<0)
	            largest = l;	
	 
	        // Si el hijo de la derecha es mayor a la ra�z.
	        if (r < n && arr[r].compareTo(arr[largest])<0)
	            largest = r;
	 
	        // Si el mayor no es la ra�z
	        if (largest != raiz)
	        {
	            T swap = arr[raiz];
	            arr[raiz] = arr[largest];
	            arr[largest] = swap;
	 
	            // Aplica recursividad.
	            heapify(arr, n, largest);
	        }
	    }
	    
	    
	    void heapifyI(T arr[], int n, int i)
	    {
	        int largest = i;  // Inicializa el mayor como la ra�z.
	        int l = 2*i + 1;  // left = 2*i + 1
	        int r = 2*i + 2;  // right = 2*i + 2
	 
	        // Si el hijo de la izquierda es mayor a la ra�z.
	        if (l < n && arr[l].compareTo(arr[largest])>0)
	            largest = l;	
	 
	        // Si el hijo de la derecha es mayor a la ra�z.
	        if (r < n && arr[r].compareTo(arr[largest])>0)
	            largest = r;
	 
	        // Si el mayor no es la ra�z
	        if (largest != i)
	        {
	            T swap = arr[i];
	            arr[i] = arr[largest];
	            arr[largest] = swap;
	 
	            // Aplica recursividad.
	            heapify(arr, n, largest);
	        }
	    }

}
