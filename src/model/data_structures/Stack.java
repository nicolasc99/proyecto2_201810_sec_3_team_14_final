package model.data_structures;

import java.util.Iterator;


public class Stack <T extends Comparable<T>> implements IStack<T>{

	/**
	 * El Nodo que contiene el primer elemento de la lista 
	 * en este caso el primero es el mas antiguo.
	 */
	private Node<T> first;
	/**
	 * El Nodo que contiene el primer elemento de la lista 
	 * en este caso el ultimo  es el mas reciente. 
	 */
	private Node<T> last;
	private int size;

	/**
	 * Crea la pila con el primer nodo sin objeto sin siguiente y sin anterior
	 * cuando la pila es creada como no hay elementos se toma el primero como el ultimo.
	 */
	public Stack()
	{
		first= null;
		last=null;
		size=0;
	}

	/**
	 * A�ade un elemento al final de  la pila 
	 * este es referenciado como el ultimo, es decir el mas reciente.
	 * Si la pila esta vacia a�ade un elemento al primer nodo que ya esta creado.
	 * @param obj objeto que contiene el nodo.
	 */
	public void push(T obj) {
		if(first!=null)
		{
			Node<T> n= new Node<T>(obj,null,last);
			last.changeNext(n);
			last=last.next();
			size++;
		}
		else
		{
			first= new Node<T>(obj,null,null);
			last=first;
			size++;
		}	

	}

	/**
	 * Elimina el ultimo elemento de la pila, es decir el mas reciente.
	 * @return El objeto que saco de la cola 
	 */
	@Override
	public T pop() {
		T deleted=null;
		if (last!=null)
		{

			if (last.previous()!=null)
			{
				size--;
				deleted= last.getObject();
				Node<T> newLast=last.previous();
				newLast.changeNext(null);
				last=newLast;
			}
			else 
			{
				last=null;
				first=last;
				size--;
			}
		}

		return deleted;

	}

	@Override
	public boolean isEmpty() {
		return size==0?true:false;
	}

	public T get(T obj) {
		T object=null;
		Node<T> curr=first;
		while(curr!=null && object==null)
		{
			if (curr.getObject().compareTo(obj)==0)
			{
				object=curr.getObject();
			}
			curr=curr.next();
		}
		return object;
	}


	public int size() {

		return size;
	}


	public T get(int position) {
		Node<T> curr=first;
		int j=0;
		while(curr!=null && j!= position)
		{
			curr=curr.next();
		}
		return curr.getObject();

	}


	public  Node<T> getFirst()
	{
		return first;
	}


	public Node<T> getlast() {
		return last;
	}


	public Node<T> next() {
		Node<T> node=null;
		if(last.next().getObject()!=null)
		{
			last=last.next();
			node=last;
		}
		return node;
	}

	public Iterator<T> iterator() {

		return new StackIterator();
	}
	private class StackIterator implements Iterator<T>
	{
		private Node<T> curr= last;

		public boolean hasNext() {
			return curr!=null;
		}


		public T next() {
			T following=curr.getObject();
			curr=curr.previous();
			return following;

		}
	}



}
