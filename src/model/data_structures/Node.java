package model.data_structures;

public class Node <T extends Comparable<T>> {
	
	private T object;
	private Node<T> next;
	private Node<T> prev;
	
	public Node(T pObject,Node<T> pNext, Node<T> pPrev )
	{
		object= pObject;
		next=pNext;
		prev=pPrev;
	}
	
	public T getObject()
	{
		return object;
	}
	
	public Node<T> next()
	{
		return next;
	}

	public Node<T> previous()
	{
		return prev;
	}
	
	public void changeObject(T obj)
	{
		object= obj;
	}
	
	
	public void changeNext(Node<T> pN)
	{
		next=pN;
	}
	
	public void changePrevious(Node<T> pP)
	{
		prev=pP;
	}
	
}
