package model.data_structures;

import java.util.Comparator;

import model.vo.Servicio;

public class RedBlackTree<Key extends Comparable <Key> , Value extends Comparable<Value>> {

	private static final boolean RED   = true;
	private static final boolean BLACK = false;

	private NodeTree<Key, Value> root;

	public RedBlackTree()
	{
		root=null;
	}
	public boolean isEmpty() {
		return root == null;
	}

	public Value get ( Key k , Comparator<Key> c)
	{
		NodeTree<Key, Value> nod=root;
		while(nod!=null)
		{
			int cmp= c.compare(k,nod.getKey());
			if (cmp<0) 
				nod= nod.getLeft();
			else if (cmp>0)
				nod=nod.getRight();
			else 
				return nod.getValue(); 

		}
		return null;
	}

	public boolean contains(Key key , Comparator<Key> c) {
		return get(key, c) != null;
	}

	public NodeTree<Key, Value> getRoot()
	{
		return root;
	}

	public boolean isRed(NodeTree<Key, Value> n)
	{
		if(n==null)
			return false;
		return n.getColor();
	}

	private int size(NodeTree<Key,Value> x) {
		if (x == null) return 0;
		return x.getSize();
	} 


	/**
	 * Returns the number of key-value pairs in this symbol table.
	 * @return the number of key-value pairs in this symbol table
	 */
	public int size() {
		return size(root);
	}

	public NodeTree<Key,Value> rotateRight (NodeTree <Key,Value> parent)
	{
		NodeTree<Key,Value> son= parent.getLeft();
		parent.changeLeft(son.getRight());
		son.changeRight(parent);
		son.setColor(isRed(parent));
		parent.setColor(true);
		son.setSize(parent.getSize());

		return son;
	}


	public NodeTree<Key,Value> rotateLeft (NodeTree <Key,Value> parent)
	{
		NodeTree<Key,Value> son= parent.getRight();
		parent.changeRight(son.getLeft());
		son.changeLeft(parent);
		son.setColor(isRed(parent));
		parent.setColor(true);
		son.setSize(parent.getSize());
		parent.setSize(size(parent.getLeft())+ size(parent.getRight())+1);
		return son;
	}

	public void flipColors(NodeTree<Key,Value> n)
	{
		n.setColor(!isRed(n));
		n.getLeft().setColor(!isRed(n.getLeft()));
		n.getRight().setColor(!isRed(n.getRight()));
	}
	public void put(Key key, Value val, Comparator c) {
		if (key == null)
			throw new IllegalArgumentException("first argument to put() is null");
		if (val == null) {
			throw new NullPointerException("El valor es nulo");

		}
		root = put(root, key, val,c);
		root.setColor(BLACK);

	}

	private NodeTree<Key,Value> put(NodeTree<Key,Value> n, Key pKey, Value val,Comparator<Key> c){

		if (n==null)
			return new NodeTree<Key,Value>(pKey,val,1,RED);
		int comp= c.compare(pKey, n.getKey());
		if(comp<0)
		{

			n.changeLeft(put(n.getLeft(),pKey,val,c));
		}
		else if (comp>0)
			n.changeRight(put(n.getRight(),pKey,val,c ));
		else
			n.changeValue(val);
		if(isRed(n.getRight()) && !isRed(n.getLeft()))
			n=rotateLeft(n);
		if(isRed(n.getLeft()) && isRed(n.getLeft().getLeft()))
			n=rotateRight(n);
		if(isRed(n.getRight()) && isRed(n.getLeft()))
			flipColors(n);

		return n;
	}




	public Key min() throws Exception {
		if (isEmpty()) throw new Exception("calls min() with empty symbol table");
		return min(root).getKey();
	}
	private NodeTree<Key,Value> min(NodeTree<Key, Value> x) { 
		// assert x != null;
		if (x.getLeft() == null) return x; 
		else   
			return min(x.getLeft());
	}  

	public Key max() throws Exception {
		if (isEmpty()) throw new Exception("calls max() with empty symbol table");
		return max(root).getKey();
	}

	private NodeTree<Key,Value> max(NodeTree<Key,Value> x) { 
		// assert x != null;
		if (x.getRight() == null) return x; 
		else                 return max(x.getRight()); 

	}

	//    public Iterable<Key> keys() throws Exception {
	//        if (isEmpty()) return new Queue<Key>();
	//        return keys(min(), max());
	//        }

	public Iterable<Key> keys(Key lo, Key hi) {
		if (isEmpty()) return new Queue<Key>();
		if (lo == null) throw new IllegalArgumentException("first argument to keys() is null");
		if (hi == null) throw new IllegalArgumentException("second argument to keys() is null");

		Queue<Key> queue = new Queue<Key>();
		// if (isEmpty() || lo.compareTo(hi) > 0) return queue;
		keys(root, queue, lo, hi);
		return queue;
	}

	// add the keys between lo and hi in the subtree rooted at x
	// to the queue
	private void keys(NodeTree<Key,Value> x, Queue<Key> queue, Key lo, Key hi) { 
		if (x == null) return; 
		int cmplo = lo.compareTo(x.getKey()); 
		int cmphi = hi.compareTo(x.getKey()); 
		if (cmplo < 0) keys(x.getLeft(), queue, lo, hi); 
		if (cmplo <= 0 && cmphi >= 0) queue.enqueue(x.getKey()); 
		if (cmphi > 0) keys(x.getRight(), queue, lo, hi); 
	}


	/**
	 * Recorre un arbol y retorna una lista que contiene los valores del arbol que estan dentro 
	 * del rango dado por parametro 
	 * @param x Nodo en el cual se desea empezar la busqueda 
	 * @param lista Lista en la cual se van a guardar los valores del arbol que estan dentro 
	 * del rango dado por parametro
	 * @param lo Limite inferior del rango 
	 * @param hi Limite superior del rango 
	 */
	public void iterate( NodeTree<Key, Value> x, LinkedList<Value> lista, Key lo, Key hi) {
		if (x != null)
		{
			int cmplo = lo.compareTo(x.getKey()); 
			int cmphi = hi.compareTo(x.getKey()); 
			if (cmplo < 0) 
			{
				iterate(x.getLeft(), lista , lo, hi); 
			}
			if (cmplo <= 0 && cmphi >= 0) 
			{
				lista.add(x.getValue());
			}
			if (cmphi > 0) 
			{
				iterate(x.getRight(), lista, lo, hi); 
			}
		}
	}
	
	
	public void idSearch( NodeTree<Key, Value> x, LinkedList<Value> lista, Key lo) {
		if (x != null)
		{
			int cmplo = lo.compareTo(x.getKey()); 
			if (cmplo < 0) 
			{
				idSearch(x.getLeft(), lista , lo); 
			}
			else if (cmplo > 0) 
			{
				idSearch(x.getRight(), lista, lo); 
			}
			else 
			{
				lista.add(x.getValue());
				
			}
		}
	}
	

}


