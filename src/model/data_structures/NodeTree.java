package model.data_structures;

public class NodeTree<Key,Value > {
	private Key key;
	private Value val;
	private NodeTree<Key, Value> right,left;
	private boolean color;
	private int size;
	
	public NodeTree(Key pKey, Value pVal, int N, boolean pColor)
	{
		key = pKey;
		val = pVal;
		size = N;
		color = pColor;
	}

	public Key getKey()
	{
		return key;
	}

	public Value getValue()
	{
		return val;
	}

	public NodeTree<Key, Value> getRight()
	{
		return right;
	}

	public NodeTree<Key, Value> getLeft()
	{
		return left;
	}

	public int getSize()
	{
		return size;
	}

	public void changeLeft(NodeTree <Key, Value> n)
	{
		left=n;
	}

	public void changeRight(NodeTree <Key, Value> n)
	{
		right=n;
	}

	public void changeValue(Value pVal)
	{
		val=pVal;
	}

	public void setColor(boolean pColor)
	{
		color=pColor;
	}

	public boolean getColor()
	{
		return color;
	}

	public void setSize(int n)
	{
		size= n;
	}



}

