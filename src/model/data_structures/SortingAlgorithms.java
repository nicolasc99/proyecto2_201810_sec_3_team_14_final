package model.data_structures;

import java.util.Comparator;

public class SortingAlgorithms <T extends Comparable <T> > {

	
	
	/**
	 * Ordena los elementos de la lista por su criterio de comparaci�n
	 * @param lista Lista que se desea ordenar 
	 * @param bool boleano que determina si la lista se ordena en que orden se ordena 
	 * Si el boleano es true se ordena de menor a mayor si es false de mayor a menor 
	 * @return lista ordenada 
	 */

	public List<T> mergeSort(List<T> lista, boolean bool, Comparator<T> pComp )
	{
		List result= new List();
//		System.out.println("El tama�o de la lista es " + lista.size());
//		System.out.println("Los elementos de la lista son------------ ");
//		for(int i=0; i<lista.size(); i++)
//		{
//			System.out.println(lista.get(i));
//		}
		//Caso base la lista tiene un solo elemento 
		if(lista.size()<=1)
		{
//			System.out.println("Hizo return porque la lista tenia un solo elemento");
			return lista;
		}
		else 
		{
			//Divide la lista en dos si tiene un numero impar de elementos redondea hacia abajo 

			int middle = lista.size()/2;

			// Nodo que esta en la mitad 
			Node<T> Nmiddle= lista.getNode(middle-1);

			//Nodo que esta despues de la mitad 
			Node<T> NafterMiddle= lista.getNode(middle);

			//Crea una lista para la parte derecha y le a�ade el nodo que esta despues de la mitad
			List<T> right= new List<T>();
			right.addNode(NafterMiddle);
			right.setFirst(NafterMiddle);

			//Le asigna como ultimo el ultimo nodo de la lista que llego 
			right.setLast(lista.getLast());

			//System.out.println(right.size() +" tam derecha");
			//System.out.println(right.getFirst().getObject() + "objeto derecha");
			//Independiza las dos listas 
			//cambia el primero de la lista derecha a null
			right.getFirst().changePrevious(null);
			Nmiddle.changeNext(null);
			lista.setLast(Nmiddle);

			//Cambio de tama�os de las listas 
			right.setSize(lista.size()-middle);
			lista.setSize(middle);

			//Uso de la recursion 

			lista=mergeSort(lista,bool,pComp);
			right=mergeSort(right,bool,pComp);
			if(bool)
			lista= merge(lista, right, pComp);
			else 
				lista=mergeInverse(lista, right, pComp);
				
//			System.out.println("\n"+ "lista queda ");
//			for(int i=0; i<result.size(); i++)
//			{
//				System.out.println(result.get(i));
//				
//			}
//			System.out.println("El tama�o de esta lista es "+result.size());

			
		}
		return lista;
	}
	
	

	public List<T> merge(List<T> left, List<T> right, Comparator<T> pComparator)
	{
		
		List<T> result= new List<T>();
		while(left.size()>0 && right.size()>0)
		{
			
			if(pComparator.compare(left.getFirst().getObject(), right.getFirst().getObject())<1)
			{
				result.addNode(left.removeFirst());
			}
			else
				result.addNode(right.removeFirst());
		}
		while(left.size()>0 )
		{
			result.addNode(left.removeFirst());
		}
		while(right.size()>0)
		{
			result.addNode(right.removeFirst());
		}
		return result;	
	}
	
	
	public List<T> mergeInverse(List<T> left, List<T> right, Comparator<T> pComparator)
	{
		
		List<T> result= new List<T>();
		while(left.size()>0 && right.size()>0)
		{
			
			if(pComparator.compare(left.getFirst().getObject(), right.getFirst().getObject())<1)
			{
				result.addNode(right.removeFirst());
			}
			else
				result.addNode(left.removeFirst());
		}
		while(left.size()>0 )
		{
			result.addNode(left.removeFirst());
		}
		while(right.size()>0)
		{
			result.addNode(right.removeFirst());
		}
		return result;	
	}


}
