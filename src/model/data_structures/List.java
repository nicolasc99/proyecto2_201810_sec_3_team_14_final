package model.data_structures;

import java.util.Comparator;
import java.util.Iterator;

import model.vo.Servicio;
//La lista ya es comparable, recuerda que es de tipo T y ese tipo es el comparable, ahora lo �nico que hay que hacer es implementar un comparador :):v
// M�tele un comparador gen�rico! :)

public class List <T extends Comparable <T>> implements Comparable<List<T>>, LinkedList<T> {

	private Node<T> first;
	private Node<T> last;
	private int size;


	public List()
	{
		first= null;
		last=null;
		size=0;
	}


	public void addNode(Node<T> node)
	{
		if(first!=null)
		{

			last.changeNext(node);
			last=last.next();
			size++;

		}
		else
		{
			first=node;
			last=first;
			size++;
		}

	}
	@Override
	public void add(T obj) {
		if(first!=null)
		{

			Node<T> n= new Node<T>(obj,null,last);
			last.changeNext(n);
			last=last.next();
			size++;

		}
		else
		{
			Node<T> n = new Node<T>(obj,null,null);
			first=n;
			last=first;
			size++;
		}

	}

	@Override
	public T get(T obj) {
		T object=null;
		Node<T> curr=first;
		while(curr!=null && object==null)
		{
			if (curr.getObject().compareTo(obj)==0)
			{
				object=curr.getObject();
			}
			curr=curr.next();
		}
		return object;
	}

	@Override
	public int size() {

		return size;
	}

	@Override
	public T get(int pos) {
		Node<T> nod = null;
		if(pos>=0 && pos<size)
		{
			if (pos == 0 && first != null)
			{
				return  first.getObject();

			}
			else if(first == null)
			{
				return null;
			}
			else
			{
				nod = first;
				for (int i = 0; i < pos; i++) {
					nod = nod.next();
				}
				return  nod.getObject();
			}
		} 
		else
		{
			return null;
		}
	}




	/**
	 * Este metodo retorna el nodo en la posicion dada
	 * @param pos posicion del nodo que se esta buscando-
	 * Si el nodo no existe retrona null
	 * @return nodo que esta en la posicion indicada 
	 */
	public Node<T> getNode(int pos) {
		Node<T> nod = null;
		if(pos>=0 && pos<size)
		{
			if (pos == 0 && first != null)
			{
				return  first;

			}
			else if(first == null)
			{
				return null;
			}
			else
			{
				nod = first;
				for (int i = 0; i < pos; i++) {
					nod = nod.next();
				}
				return  nod;
			}
		} 
		else
		{
			return null;
		}

	}

	/**
	 * Intercambia elementos en la lista
	 * @param pos1 posicion a la que esta el objeto 1 
	 * @param pos2 posicion en la que esta el objeto 2 
	 */
	public void exchange(int pos1,int pos2)
	{
		Node<T> nod1= getNode(pos1);
		Node<T> nod2= getNode(pos2);
		if(nod1!=null && nod2!=null)
		{
			T objNod1= nod1.getObject();
			T objNod2= nod2.getObject();
			nod1.changeObject(objNod2);
			nod2.changeObject(objNod1);
		}

	}



	public boolean contains(T nodo)
	{		
		for(T nodito: this)

		{
			if(nodito!=null &&nodito.compareTo(nodo)==0)
			{
				return true;
			}
		}
		return false;		
	}




	@Override
	public void lisiting() {

		last=first;

	}

	public  Node<T> getFirst()
	{
		return first;
	}

	public Node<T> getLast() {
		return last;
	}

	@Override
	public Node<T> next() {
		Node<T> node=null;
		if(last.next().getObject()!=null)
		{
			last=last.next();
			node=last;
		}
		return node;
	}

	public void setFirst(Node<T> pfirst)
	{
		first= pfirst;
	}

	public void setSize(int pSize)
	{
		size=pSize;
	}

	public void setLast(Node<T> pLast)
	{
		last= pLast;
	}

	public Node<T> removeFirst()
	{
		Node<T> oldFirst=null;
		if(first!=null)
		{
			if(first.next()==null)
			{
				oldFirst=first;
				first=null;
				last=null;
				size--;
			}
			else
			{
				oldFirst=first;
				first=first.next();
				first.changePrevious(null);
				oldFirst.changeNext(null);
				size--;
			}
		}
		return oldFirst;
	}

	//Por el momento ignora este metodo solo lo puse aqui para probar algo 
	public List ShellSort(List pLista)
	{
		int N= pLista.size();
		int h=1;
		while (h<N/3) h=3*h+1;
		while (h>=1)
		{
			for(int i=h ;i<N;i++)
			{
				for (int j=i; j>h&& pLista.get(j).compareTo(pLista.get(j-h))==-1;j-=h)
				{
					pLista.exchange(j, j-h);
				}
				h=h/3;
			}
		}
		return pLista;
	}
	
	
	
	public List<T> sort(Comparator<T> pComparador)
	{
		
		SortingAlgorithms<T> mergeador = new SortingAlgorithms<T>();
		
		return mergeador.mergeSort((List<T>) this, true, pComparador );
		
		
	}

	
	@Override
	public Iterator<T> iterator() {

		return new ListIterator();
	}
	private class ListIterator implements Iterator<T>
	{
		private Node<T> curr= first;
		@Override
		public boolean hasNext() {
			return curr!=null;
		}

		@Override
		public T next() {
			T next=curr.getObject();
			curr=curr.next();
			return next;

		}
	}
	@Override
	public int compareTo(List<T> o) {
		// TODO Auto-generated method stub
		return 0;
	}
}
