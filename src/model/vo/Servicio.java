package model.vo;

import java.util.Date;

/**
 * Representation of a Service object
 */
public class Servicio implements Comparable<Servicio>
{	


	private String trip_id;
	private String taxi_id;
	private int trip_seconds;
	private double trip_miles;
	private double trip_total;
	private int dropoff_community_area;
	private Date trip_start_timestamp;
	private Date trip_end_timestamp;
	private String company;
	private double pickup_centroid_latitude;
	private double pickup_centroid_longitude;
	private double distancia;
	private int pickup_community_area;
	private double fare;

	public Servicio (String trip, String taxi, int seconds, double miles, String comp,int pickup, double pFare )
	{
		trip_id= trip;
		taxi_id=taxi;
		trip_seconds= seconds;
		trip_miles= miles;
		company= comp;
		pickup_community_area=pickup;
		fare=pFare;
	}

	
	public double getDistance (double lat1, double lon1, double lat2, double lon2)
	{
	 // TODO Auto-generated method stub
	 final int R = 6371*1000; // Radious of the earth in meters
	 Double latDistance = Math.toRadians(lat2-lat1);
	 Double lonDistance = Math.toRadians(lon2-lon1);
	 Double a = Math.sin(latDistance/2) * Math.sin(latDistance/2) + Math.cos(Math.toRadians(lat1))
	 * Math.cos(Math.toRadians(lat2)) * Math.sin(lonDistance/2) * Math.sin(lonDistance/2);
	 Double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
	 Double distance = R * c;
	 return distance*0.000621371;
	}
	
	public boolean isInRange(RangoFechaHora rango)
	{
		
		if(getDropoff_end_timestamp()!=null&&getDropoff_start_timestamp().before(rango.getFechaFinal())&&getDropoff_end_timestamp().after(rango.getFechaInicial()))
			
		{
			return true;
			
				
		}
		
		return false;
		
	}
	
	
	public void setTrip_id(String trip_id) {
		this.trip_id = trip_id;
	}

	

	public void setTaxi_id(String taxi_id) {
		this.taxi_id = taxi_id;
	}


	public void setTrip_seconds(int trip_seconds) {
		this.trip_seconds = trip_seconds;
	}


	public void setTrip_miles(double trip_miles) {
		this.trip_miles = trip_miles;
	}


	public void setTrip_total(double trip_total) {
		this.trip_total = trip_total;
	}


	public void setDropoff_community_area(int dropoff_community_area) {
		this.dropoff_community_area = dropoff_community_area;
	}


	public void setTrip_start_timestamp(Date trip_start_timestamp) {
		this.trip_start_timestamp = trip_start_timestamp;
	}


	public void setTrip_end_timestamp(Date trip_end_timestamp) {
		this.trip_end_timestamp = trip_end_timestamp;
	}


	public void setCompany(String company) {
		this.company = company;
	}


	public void setPickup_community_area(int pickup_community_area) {
		this.pickup_community_area = pickup_community_area;
	}


	public void setFare(double fare) {
		this.fare = fare;
	}

	
	
	/**
	 * @return id - Trip_id
	 */
	public String getTripid() {
		// TODO Auto-generated method stub
		return trip_id;
	}	


	public String getCompany() {
		// TODO Auto-generated method stub
		return company;
	}	
	
	public double getDistancia() {
		// TODO Auto-generated method stub
		return distancia;
	}	
	
	
	public void setDistancia(double pDistancia) {
		// TODO Auto-generated method stub
		this.distancia = pDistancia;
	}	


	public Date getDropoff_start_timestamp()
	{
		// TODO Auto-generated method stub
		return trip_start_timestamp;

	}	


	public Date getDropoff_end_timestamp() {
		// TODO Auto-generated method stub
		return trip_end_timestamp;

	}	


	public int getDropoff_community_area() {
		return dropoff_community_area;
	}

	/**
	 * @return id - Taxi_id
	 */
	public String getTaxiid() {
		// TODO Auto-generated method stub
		return taxi_id;
	}	

	/**
	 * @return time - Time of the trip in seconds.
	 */
	public int getTripSeconds() {
		// TODO Auto-generated method stub
		return trip_seconds;
	}

	/**
	 * @return miles - Distance of the trip in miles.
	 */
	public double getTripMiles() {
		// TODO Auto-generated method stub
		return trip_miles;
	}

	/**
	 * @return total - Total cost of the trip
	 */
	public double getTripTotal() {
		// TODO Auto-generated method stub
		return trip_total;
	}
	
	public int getPickup_community_area()
	{
		return pickup_community_area;
	}
	
	public double getFare()
	{
		return fare;
	}




	@Override
	public int compareTo(Servicio o) {
		// TODO Auto-generated method stub
		if(o.getPickup_community_area()==(getPickup_community_area()))
		{
			return 0;
		}
		
		if(o.getPickup_community_area()<(getPickup_community_area()))
		{
			return 1;
			
		}
		
		return -1;
		
	}
	
	
	public String toString()
	{
		return String.format("Service Id: %s TaxiId: %s Latitud: %s Longitud: %s Zona de recogida: %s Zona de llegada: %s Fecha: %s " ,trip_id, taxi_id, pickup_centroid_latitude,pickup_centroid_longitude,  pickup_community_area,dropoff_community_area,trip_start_timestamp);
	}
	
	public String format()
	{
		return String.format("Service Id: %s Millas recorridas %s" , trip_id, trip_miles );
	}



	public int getPickupZone() {
		// TODO Auto-generated method stub
		return pickup_community_area;
	}



	public int getDropOffZone() {
		// TODO Auto-generated method stub
		return dropoff_community_area;
	}



	public String getStartTime() {
		// TODO Auto-generated method stub
		return trip_start_timestamp.getHours() +":" +trip_start_timestamp.getMinutes();
	}



	public double getCentroidLatitud() {
		// TODO Auto-generated method stub
		return pickup_centroid_latitude;
	}



	public double getCentroidLongitud() {
		// TODO Auto-generated method stub
		return pickup_centroid_longitude;
	}

	
}

