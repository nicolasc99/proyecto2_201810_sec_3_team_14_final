package model.vo;

import model.data_structures.LinkedList;
import model.data_structures.List;

/**
 * VO utilizado en Req 5A, tiene el rango de distancia y la lista de servicios cuya distancia recorrida 
 * pertenece a dicho rango
 */
public class RangoDuracion implements Comparable<RangoDuracion>
{
	//ATRIBUTOS
			
    /**
     * Modela el valor minimo del rango
     */
	private double limiteSuperior;
	
	/**
	 * Modela el valor m�ximo del rango
	 */
	private double limineInferior;
	
	/**
	 * Modela la lista de servicios cuya distancia recorrida esta entre el l�mite inferior y el l�mite superior
	 */
	private LinkedList<Servicio> serviciosEnRango;
	
	
	
	public RangoDuracion(double pSuperior, double pInferior)
	{
		limiteSuperior=pSuperior;
		limineInferior=pInferior;
		serviciosEnRango = new List<Servicio>();
	}

	//M�TODOS
	
	/**
	 * @return the limiteSuperior
	 */
	public double getLimiteSuperior()
	{
		return limiteSuperior;
	}
	
	public void add(Servicio T)
	{
		serviciosEnRango.add(T);
	}

	
	/**
	 * @param limiteSuperior the limiteSuperior to set
	 */
	public void setLimiteSuperior(double limiteSuperior) 
	{
		this.limiteSuperior = limiteSuperior;
	}

	/**
	 * @return the limineInferior
	 */
	public double getLimineInferior() 
	{
		return limineInferior;
	}

	/**
	 * @param limineInferior the limineInferior to set
	 */
	public void setLimineInferior(double limineInferior) 
	{
		this.limineInferior = limineInferior;
	}

	/**
	 * @return the serviciosEnRango
	 */
	public LinkedList<Servicio> getServiciosEnRango() 
	{
		return serviciosEnRango;
	}

	/**
	 * @param serviciosEnRango the serviciosEnRango to set
	 */
	public void setServiciosEnRango(LinkedList<Servicio> serviciosEnRango)
	{
		this.serviciosEnRango = serviciosEnRango;
	}

	@Override
	public int compareTo(RangoDuracion o)
	{
		return 0;
	}
}
