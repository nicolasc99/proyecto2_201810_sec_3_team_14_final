package model.vo;

import model.data_structures.List;
import model.data_structures.SeparateChaining;

/**
 * Representation of a taxi object
 */
public class Taxi implements Comparable<Taxi>
{
	private SeparateChaining< Integer, List<Servicio>> serviciosArea;
	private List<Servicio> serviciosInscritos;
	private List<Servicio> serviciosInscritosArea;

	
	String taxi_id;
	
	String company;
	
	
	private List<Servicio> serviciosInscritosFecha;

	
	public Taxi(String pCompany, String pTaxi_Id)
	{
		serviciosInscritos = new List<Servicio>();
		serviciosInscritosFecha = new List<Servicio>();
		serviciosArea = new SeparateChaining<>();
		serviciosInscritosArea = new List<Servicio>();

		taxi_id=pTaxi_Id;
		company= pCompany;
	}
	
	/**
	 * @return id - taxi_id
	 */
	public String getTaxiId() {
		// TODO Auto-generated method stub
		return taxi_id;
	}
	
	
	
	public List<Servicio> getServiciosInscritos() {
		return serviciosInscritos;
	}
	
	public List<Servicio> getServiciosInscritosArea() {
		return serviciosInscritosArea;
	}
	
	
	public SeparateChaining<Integer ,List<Servicio>> getServiciosArea() {
		return serviciosArea;
	}


	
	public List<Servicio> getServiciosInscritosFecha() {
		return serviciosInscritosFecha;
	}

	public void add1(Servicio T)
	{
		serviciosInscritosFecha.add(T);
	}
	
	 public void print(){
	        System.out.println(Integer.toString(numeroServicios())+"servicios "+"Taxi: "+taxi_id);
	        for(Servicio s : serviciosInscritosArea){
	            System.out.println("\t"+s.getStartTime());
	        }
	        System.out.println("___________________________________");;
	    }
	
	 
	 
	 public int numeroServicios(){
	        return serviciosArea.size();
	    }
	 
	public double dardistanciaRecorrida()
	{
		double total=0;
		
		for(Servicio servicito: serviciosInscritos)
		{
			total+=servicito.getTripMiles();
			
		}
		
		return total;
		
	}
	
	public double darServiciosPuntos()
	{
		double total=0;
		
		for(Servicio servicito: serviciosInscritos)
		{

			if(servicito.getTripTotal()>0&&servicito.getTripMiles()>0)

			{
				total++;

			}
			
		}
		
		return total;
		
	}
	public double darrentabilidad()
	
	{
		
		return (darplataGanada()/dardistanciaRecorrida())*darServiciosPuntos();
		
	}

	
	public double darplataGanada()
	{
		double total=0;
		
		for(Servicio servicito: serviciosInscritos)
		{
			total+=servicito.getTripTotal();
			
		}
		
		return total;
		
	}
	
	
	public double darPuntos()
	{
		
		return (dardistanciaRecorrida()/darplataGanada());
		
	}
	
	public double darplataGanadaF()
	{
		double total=0;
		
		for(Servicio servicito: serviciosInscritosFecha)
		{
			if(servicito!=null)
			total+=servicito.getTripTotal();
			
		}
		
		return total;
		
	}
	
	
	public double darDistanciaF()
	{
		double total=0;
		
		for(Servicio servicito: serviciosInscritosFecha)
		{
			if(servicito!=null)
			total+=servicito.getTripMiles();
			
		}
		
		return total;
		
	}


	

	public void setServiciosInscritosF(List<Servicio> pserviciosInscritos) {
		this.serviciosInscritos = pserviciosInscritos;
	}
	
	public void add(Servicio T)
	{
		serviciosInscritos.add(T);
	}

	
	public void setServiciosInscritos(List<Servicio> pserviciosInscritos) {
		this.serviciosInscritos = pserviciosInscritos;
	}

	
	/**
	 * @return company
	 */
	public String getCompany() {
		// TODO Auto-generated method stub
		return company;
	}


	@Override
	public int compareTo(Taxi o) {
		// TODO Auto-generated method stub
		return o.getTaxiId().compareTo(taxi_id);
	}	
}
