package model.vo;
import model.data_structures.LinkedList;
import model.data_structures.List;

public class RangoDistancia implements Comparable<RangoDistancia>{
	

		//ATRIBUTOS
				
	    /**
	     * Modela el valor minimo del rango
	     */
		private double limiteSuperior;
		
		/**
		 * Modela el valor m�ximo del rango
		 */
		private double limiteInferior;
		
		/**
		 * Modela la lista de servicios cuya distancia recorrida esta entre el l�mite inferior y el l�mite superior
		 */
		private LinkedList<Servicio> serviciosEnRango;
		
		
		
		public RangoDistancia(double pSuperior, double pInferior)
		{
			limiteSuperior=pSuperior;
			limiteInferior=pInferior;
			serviciosEnRango = new List<Servicio>();
		}

		//M�TODOS
		
		/**
		 * @return the limiteSuperior
		 */
		public double getLimiteSuperior()
		{
			return limiteSuperior;
		}
		
		public void add(Servicio T)
		{
			serviciosEnRango.add(T);
		}

		
		/**
		 * @param limiteSuperior the limiteSuperior to set
		 */
		public void setLimiteSuperior(double limiteSuperior) 
		{
			this.limiteSuperior = limiteSuperior;
		}

		/**
		 * @return the limineInferior
		 */
		public double getLimineInferior() 
		{
			return limiteInferior;
		}

		/**
		 * @param limineInferior the limineInferior to set
		 */
		public void setLimineInferior(double limineInferior) 
		{
			this.limiteInferior = limineInferior;
		}

		/**
		 * @return the serviciosEnRango
		 */
		public LinkedList<Servicio> getServiciosEnRango() 
		{
			return serviciosEnRango;
		}

		/**
		 * @param serviciosEnRango the serviciosEnRango to set
		 */
		public void setServiciosEnRango(LinkedList<Servicio> serviciosEnRango)
		{
			this.serviciosEnRango = serviciosEnRango;
		}

		@Override
		public int compareTo(RangoDistancia o) {
			// TODO Auto-generated method stub
			return 0;
		}

		
	}



