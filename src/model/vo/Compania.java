 package model.vo;

import model.data_structures.List;

public class Compania implements Comparable<Compania> {
	
	
	private List<Taxi> taxisInscritos;
	
	String company;
	int numServicios;
	

	
	public Compania(String pNombre)
	{
		
		company=pNombre;
		taxisInscritos= new List<Taxi>();
		numServicios=0;
		
		
	}
	
	
	public String getNombre() {
		return company;
	}

	public void setNombre(String nombre) {
		this.company = nombre;
	}

	public List<Taxi> getTaxisInscritos() {
		return taxisInscritos;
	}
	
	public int getNumServicios()
	{
		return numServicios;
	}
	
	public void add(Taxi T)
	{
		taxisInscritos.add(T);
		numServicios+=T.getServiciosInscritos().size();
	}

	public void setTaxisInscritos(List<Taxi> taxisInscritos) {
		this.taxisInscritos = taxisInscritos;
	}
 
	@Override
	public int compareTo(Compania o) {
		// TODO Auto-generated method stub
		return o.getNombre().compareTo(company);
	}
	
	
	

}
