package model.vo;

import java.util.Date;

/**
 * Modela una rango de fechas y horas (iniciales y finales)
 *
 */
public class RangoFechaHora
{
	//ATRIBUTOS
	
    /**
     * Modela la fecha inicial del rango
     */
	private Date fechaInicial; 
	
	/**
	 * Modela la fecha final del rango
	 */
	private Date fechaFinal;
	

	
	//CONSTRUCTOR
	/**
	 * @param pFechaInicial, fecha inicial del rango
	 * @param pFechaFinal, fecha final del rango
	 */
	public RangoFechaHora(Date pFechaInicial, Date pFechaFinal)
	{
		fechaInicial = pFechaInicial;
		fechaFinal = pFechaFinal;


	}
	//M�TODOS
	
	/**
	 * @return the fechaInicial
	 */
	public Date getFechaInicial() 
	{
		return fechaInicial;
	}

	/**
	 * @param fechaInicial the fechaInicial to set
	 */
	public void setFechaInicial(Date fechaInicial)
	{
		this.fechaInicial = fechaInicial;
	}

	/**
	 * @return the fechaFinal
	 */
	public Date getFechaFinal() 
	{
		return fechaFinal;
	}

	/**
	 * @param fechaFinal the fechaFinal to set
	 */
	public void setFechaFinal(Date fechaFinal) 
	{
		this.fechaFinal = fechaFinal;
	}

}