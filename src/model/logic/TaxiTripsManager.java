package model.logic;

import java.io.FileReader;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import api.ITaxiTripsManager;
import model.data_structures.HeapSort;
import model.data_structures.IStack;
import model.data_structures.List;
import model.data_structures.Node;
import model.data_structures.NodeTree;
import model.data_structures.Queue;
import model.data_structures.RedBlackTree;
import model.data_structures.SeparateChaining;
import model.data_structures.SortingAlgorithms;
import model.data_structures.Stack;
import model.vo.Taxi;
import model.vo.TaxiConPuntos;
import model.vo.TaxiConServicios;
import model.vo.Compania;
import model.vo.InfoTaxiRango;
import model.vo.RangoDistancia;
import model.vo.RangoDuracion;
import model.vo.RangoFechaHora;
import model.vo.Servicio;

public class TaxiTripsManager implements ITaxiTripsManager {

	public static final String DIRECCION_SMALL_JSON = "./data/taxi-trips-wrvz-psew-subset-small.json";
	public static final String DIRECCION_MEDIUM_JSON = "./data/taxi-trips-wrvz-psew-subset-medium.json";
	public static final String DIRECCION_LARGE_JSON = "./data/taxi-trips-wrvz-psew-subset-large.json";

	private List<Servicio> listaServicios;
	private SeparateChaining< Integer, List<Servicio>> serviciosArea;
	private SeparateChaining <Double, List<Servicio>> serviciosMillasRound;
	private SeparateChaining<String, RedBlackTree<Date, List<Servicio>>> serviciosZonas;
	private RedBlackTree <Double, List<Servicio> > arbolServicios; 
	private RedBlackTree<Date, List<Servicio>> arbolHoras;
	private Queue<Servicio> colaServicios; 
	private Queue<Taxi> colaTaxis; 
	private List<Compania>listacompanias = new List<Compania>();
	private RedBlackTree <String, List<Taxi> > arbolCompanias;
	private SeparateChaining< RangoDuracion, List<Servicio>> serviciosRango;
	private SeparateChaining<RangoDistancia, RedBlackTree<String, Servicio>> tabla;
	Comparator<String> x= new NameComparator();
	private List<RangoDuracion> listaRangos;
	private List<RangoDistancia> listaDistancia;
	double rangomaximo =0;
	double distanciamaxima=0;


	private Servicio[] item;

	boolean bool = false;

	public boolean loadServices(String serviceFile) {



		long time=System.currentTimeMillis();

		if (!bool)
		{



			try
			{

				Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS").create();
				item= gson.fromJson(new FileReader(serviceFile), Servicio[].class);
				serviciosArea = new SeparateChaining<>();
				serviciosMillasRound= new SeparateChaining<>();
				serviciosZonas=new SeparateChaining<>();
				arbolServicios=new RedBlackTree<>();
				arbolHoras=new RedBlackTree<>();
				listacompanias = new List<Compania>();
				colaServicios = new Queue<Servicio>();
				colaTaxis = new Queue<Taxi>();
				arbolCompanias = new RedBlackTree<>();
				Compania independiente = new Compania("Independent Owner");
				serviciosRango= new SeparateChaining<>();
				listaRangos = new List<RangoDuracion>();
			 tabla = new SeparateChaining<>();



				
			
				
				
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////			
				
				for(int i=0; i<item.length;i++)
				{

					colaServicios.enqueue(item[i]);


				}


				for(Servicio service: colaServicios)
				{
					if(service.getTripMiles()>rangomaximo)
					{
						rangomaximo= service.getTripSeconds();
					}		
				}

				//				System.out.println(rangomaximo);

				for(int i=0; i<rangomaximo;i+=60)
				{

					RangoDuracion temp= new RangoDuracion((i+60), i+1);
					listaRangos.add(temp);
				}
				for(RangoDuracion ranguito: listaRangos)

					//				for(Servicio servicitos: colaServicios)
				{
					//				System.out.println(ranguito.getLimineInferior()+" "+ranguito.getLimiteSuperior());


					for(Servicio servicitos: colaServicios)
						//					for(RangoDuracion ranguito: listaRangos)
					{

						if(ranguito.getLimineInferior()<=servicitos.getTripSeconds()&&servicitos.getTripSeconds()<=ranguito.getLimiteSuperior())
						{
							//							System.out.println("A�ad�");
							ranguito.add(servicitos);

						}

					}
					//					System.out.println(ranguito.getServiciosEnRango().get(0).getTripSeconds());
					serviciosRango.put(ranguito, (List<Servicio>) ranguito.getServiciosEnRango());
					//					System.out.println(serviciosRango.get(ranguito).get(0).getTripSeconds());
				}







				for(int i=0; i<colaServicios.size();i++)
				{
					Taxi taxito = new Taxi(item[i].getCompany(),item[i].getTaxiid());

					if(!colaTaxis.contains(taxito))

					{
						colaTaxis.enqueue(taxito);

					}

				}

				for(Servicio servicito: colaServicios)
				{

					for(Taxi taxito : colaTaxis )

					{

						if(taxito.getTaxiId()!=null&&taxito.getTaxiId().equalsIgnoreCase(servicito.getTaxiid()))
						{


							taxito.add(servicito);


						}



					}


				}



				for(Taxi otroTaxi: colaTaxis)
				{	
					for(Servicio unServicio: otroTaxi.getServiciosInscritos())
					{

						if(!otroTaxi.getServiciosArea().contains(unServicio.getPickup_community_area()))
						{
							List<Servicio> tempList = new List<Servicio>();
							for(Servicio otroServicio: otroTaxi.getServiciosInscritos())
							{
								if(otroServicio.getPickup_community_area()==unServicio.getPickup_community_area())
								{
									tempList.add(otroServicio);
								}

							}
							otroTaxi.getServiciosInscritosArea().add(unServicio);
							otroTaxi.getServiciosArea().put(unServicio.getPickup_community_area(), tempList.sort(new Comparator<Servicio>()
							{

								@Override
								public int compare(Servicio o1, Servicio o2) {
									try {
										return o1.getDropoff_start_timestamp().compareTo((o2.getDropoff_start_timestamp()));
									} catch (Exception e) {
										// TODO: handle exception

										return -1;
									}

								}			
							}));


						}

					}

				}

				//				System.out.println("PRIMERO! "+colaTaxis.get(20).getServiciosArea().get(8).get(0).getDropoff_start_timestamp());
				//				System.out.println("SEGUNDO! "+colaTaxis.get(20).getServiciosArea().get(8).get(1).getDropoff_start_timestamp());
				//				System.out.println("TERCERO! "+colaTaxis.get(20).getServiciosArea().get(8).get(2).getDropoff_start_timestamp());

				//				System.out.println("SEGUNDO! "+colaTaxis.get(20).getServiciosArea().get(8).get(1).getDropoff_start_timestamp());




				for(int i=0; i<colaTaxis.size();i++)
				{
					Compania companita = new Compania(colaTaxis.get(i).getCompany());

					if(companita.getNombre()!=null&&!listacompanias.contains(companita))
					{

						listacompanias.add(companita);
					}

				}

				listacompanias.add(independiente);

				for(Compania companita: listacompanias)
				{

					for(Taxi taxito : colaTaxis )

					{

						if(taxito.getCompany()!=null&&taxito.getCompany().equalsIgnoreCase(companita.getNombre()))
						{

							companita.add(taxito);


						}

						if(taxito.getCompany()==null)
						{
							independiente.add(taxito);
						}

					}

				}


				for(int i=0;i<listacompanias.size();i++)
				{


					if(!arbolCompanias.contains(listacompanias.get(i).getNombre(),x))
					{	
						arbolCompanias.put(listacompanias.get(i).getNombre(), listacompanias.get(i).getTaxisInscritos(), x);
					}


				}			

				////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////			

				double latprom =0;
				double longprom=0;
				

				
				
				List<Servicio> listtemp = new List<Servicio>();
				
			
				for(Servicio servicito: colaServicios)
				{
					
					
					
					if(servicito.getCentroidLatitud()!=0&&servicito.getCentroidLongitud()!=0)
					{
						latprom+=servicito.getCentroidLatitud();
						longprom+=servicito.getCentroidLongitud();
						listtemp.add(servicito);
					}
					
				}
				
				
				latprom/= listtemp.size();
				longprom/= listtemp.size();

			
				
				
				for(Servicio otroServicio: listtemp)
				{
					
					otroServicio.setDistancia(otroServicio.getDistance(latprom, longprom, otroServicio.getCentroidLatitud(), otroServicio.getCentroidLongitud()));
					
					
					if(otroServicio.getDistancia()>distanciamaxima)
					{
						distanciamaxima = otroServicio.getDistancia();
						
					}
				
				}
				
				
//				System.out.println(distanciamaxima);
				for(double i=0.1; i<distanciamaxima;i+=0.1)
				{
					RangoDistancia temp= new RangoDistancia(i-0.01, i-0.1);
					listaDistancia.add(temp);
				}
				
				for(RangoDistancia ranguito: listaDistancia)

					//				for(Servicio servicitos: colaServicios)
				{
					//				System.out.println(ranguito.getLimineInferior()+" "+ranguito.getLimiteSuperior());
					RedBlackTree<String, Servicio> arbolito = new RedBlackTree<>();


					for(Servicio servicitos: colaServicios)
						//					for(RangoDuracion ranguito: listaRangos)
					{

						if(ranguito.getLimineInferior()<=servicitos.getDistancia()&&servicitos.getDistancia()<=ranguito.getLimiteSuperior())
						{
							//							System.out.println("A�ad�");
							arbolito.put(ranguito.getServiciosEnRango().get(0).getTaxiid(), servicitos, x);

						}
						

					}
					
					if(arbolito.getRoot()!=null)
					{
					
					tabla.put(ranguito, arbolito);
					}
					
					//					System.out.println(ranguito.getServiciosEnRango().get(0).getTripSeconds());
//					serviciosRango.put(ranguito, (List<Servicio>) ranguito.getServiciosEnRango());
					//					System.out.println(serviciosRango.get(ranguito).get(0).getTripSeconds());
				}
				
				
				
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////			


				bool=true;

				for(int i=0; i<item.length;i++)
				{
					//					System.out.println(item[i]);
					if(!serviciosArea.contains(item[i].getPickup_community_area()))

					{
						listaServicios = new List<>();

						for(Servicio servicito: item)
						{
							if(servicito.getPickup_community_area()==item[i].getPickup_community_area())
							{
								listaServicios.add(servicito);
							}

						}



						serviciosArea.put(item[i].getPickup_community_area(), listaServicios);

					} 

					//					if (!serviciosMillasRound.contains(item[i].getTripMiles()))
					//					{ 
					//						Comparator<Servicio> c= milesRounder();
					//						List<Servicio> listaServicios = new List<Servicio>();
					//						for (Servicio ser: item)
					//						{
					//							if (c.compare(ser, item[i])==0 )
					//							{
					//								listaServicios.add(ser);
					//							}
					//						}
					//
					//						serviciosMillasRound.put(item[i].getTripMiles(), listaServicios);
					//					}
					Comparator<Double> c= new MilesComparator();
					if(!arbolServicios.contains(item[i].getTripMiles(), c))
					{
						List<Servicio> listaA= new List<Servicio>();
						listaA.add(item[i]);	
						String s=String.format("%.1f", item[i].getTripMiles());
						s= s.replace(",", ".");
						double num = Double.parseDouble(s);


						arbolServicios.put(num, listaA, c);
					}
					else
					{
						List<Servicio> listaA= arbolServicios.get(item[i].getTripMiles(), c);
						listaA.add(item[i]);
						if(listaA.size()==0)
							System.out.println("la lista esta vacia");
						//						arbolServicios.put(item[i].getTripMiles(), listaA, c);
					}


					String tupla= item[i].getPickupZone()+"-"+item[i].getDropOffZone();
					Comparator<Date> d= new DateComparator();
					if(!serviciosZonas.contains(tupla))
					{
						//						System.out.println("No contenia "+ tupla);

						RedBlackTree<Date,List<Servicio>> arbol= new RedBlackTree<Date,List<Servicio>>();
						List<Servicio> lista= new List<Servicio>();
						lista.add(item[i]);
						arbol.put(item[i].getDropoff_start_timestamp(), lista, d);
						serviciosZonas.put(tupla, arbol);

					}
					else 
					{
						//						System.out.println("entro al else");
						//						System.out.println(tupla);
						//						System.out.println(serviciosZonas.contains(tupla));
						RedBlackTree<Date,List<Servicio>> arbol =serviciosZonas.get(tupla);
						if(!arbol.contains(item[i].getDropoff_start_timestamp(), d))
						{
							List<Servicio> lista= new List<Servicio>();
							lista.add(item[i]);
							arbol.put(item[i].getDropoff_start_timestamp(), lista, d);
						}
						else
						{
							List<Servicio> lista=arbol.get(item[i].getDropoff_start_timestamp(), d);
							lista.add(item[i]);
						}
					}
	









					int min =item[i].getDropoff_start_timestamp().getMinutes();
					SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
					Date fecha;
					String roundedMinutes= MinuteRounder(min);
					int horaFinal;
					int minutosFinal;
					int fechaFinal= item[i].getDropoff_start_timestamp().getDay();
					if(roundedMinutes.equals("/"))
					{
						if(item[i].getDropoff_start_timestamp().getHours()==23)
						{
							fechaFinal=item[i].getDropoff_start_timestamp().getDay()+1;
							horaFinal=0;
							minutosFinal=0;
						}
						else
						{
							horaFinal =item[i].getDropoff_start_timestamp().getHours()+1;
							minutosFinal=0;
						}

					}
					else
					{
						horaFinal=item[i].getDropoff_start_timestamp().getHours();
						minutosFinal=Integer.parseInt(roundedMinutes);
					}
					
					fecha= formato.parse(item[i].getDropoff_start_timestamp().getYear()+
							"-"+item[i].getDropoff_start_timestamp().getMonth()+"-"+fechaFinal+'T'+horaFinal+":"+minutosFinal);
					//System.out.println(fecha+" guardada");
					
//					System.out.println(item[i].getDropoff_start_timestamp().getYear());
//					System.out.println(item[i].getDropoff_start_timestamp()+ " llegi");
					if(!arbolHoras.contains(fecha, d))
					{
						List<Servicio> lista= new List<Servicio>();
						lista.add(item[i]);
						arbolHoras.put(fecha, lista, d);
					}
					else
					{
						List<Servicio> lista= arbolHoras.get(fecha, d);
						lista.add(item[i]);
					}
//					System.out.println(item[i].getDropoff_start_timestamp());

				}
//				System.out.println("-------------------------------------------");

//				Queue<Date> ar=(Queue<Date>) arbolHoras.keys(arbolHoras.min(), arbolHoras.max());
//				
//				for(Date ds:ar)
//				{
//					System.out.println(ds);
//				}




				//				System.out.println(serviciosArea.size());
				//				System.out.println(arbolServicios.keys(arbolServicios.min(), arbolServicios.max()));
				//				System.out.println(serviciosArea.get(0).get(1));

				//				for(int i=0; i<serviciosArea.size(); i++)
				//				{
				//					
				//					if(serviciosArea.get(i)!=null&&serviciosArea.get(i).get(0)!=null)
				//					{
				//					System.out.println(serviciosArea.get(i).get(0).getPickup_community_area());
				//					}
				//				}



				//				System.out.println(item.length);
				bool=true;
				for (int y=0; y<listaServicios.size();y++)
				{
					System.out.println(listaServicios.get(y).getTaxiid());
				}

				System.out.println("Tama�o de la Lista de Servicios: " + listaServicios.size());

				System.gc();
				return bool;




			}

			catch (Exception e)
			{
				e.printStackTrace();
				System.out.println("Hubo un error al cargar el archivo");
			}


			System.out.println("Inside loadServices with " + serviceFile);
			System.out.println("tiempo "+(System.currentTimeMillis()-time));



		}

		else

		{
			System.err.println("El archivo solamente puede ser cargado una vez en el programa");


		}

		return bool;


	}



	//-----------------------------------------
	// M�todos parte A
	//-----------------------------------------

	public List<Taxi> A1TaxiConMasServiciosEnZonaParaCompania(int zonaInicio,
			String compania) {

		boolean flag= true;
		List<Taxi> lista = new List<Taxi>();
		List<Taxi> definitiva = new List<Taxi>();
		Taxi temp = arbolCompanias.get(compania, x).get(0);


		while(flag)
		{
			for(int i=0;i<arbolCompanias.get(compania, x).size();i++)
			{
				if(arbolCompanias.get(compania,x).get(i).getServiciosArea().get(zonaInicio)!=null)
				{
					//				System.out.println(arbolCompanias.get(compania,x).get(i).getServiciosArea().get(zonaInicio).size());

					temp = arbolCompanias.get(compania,x).get(i);
					flag=false;

				}

			}
		}

		for(int i=0;i<arbolCompanias.get(compania, x).size();i++)
		{
			if(arbolCompanias.get(compania,x).get(i).getServiciosArea().get(zonaInicio)!=null)
			{


				//			System.out.println(arbolCompanias.get(compania,x).get(i).getServiciosArea().get(zonaInicio).size());

				if(arbolCompanias.get(compania,x).get(i).getServiciosArea().get(zonaInicio).size()>=temp.getServiciosArea().get(zonaInicio).size())
				{
					temp = arbolCompanias.get(compania,x).get(i);
					//				lista.add(arbolCompanias.get(compania,x).get(i));
					//
				}

			}
		}

		for(int i=0;i<arbolCompanias.get(compania, x).size();i++)
		{
			if(arbolCompanias.get(compania,x).get(i).getServiciosArea().get(zonaInicio)!=null)
			{


				//			System.out.println(arbolCompanias.get(compania,x).get(i).getServiciosArea().get(zonaInicio).size());

				if(arbolCompanias.get(compania,x).get(i).getServiciosArea().get(zonaInicio).size()==temp.getServiciosArea().get(zonaInicio).size())
				{
					definitiva.add(arbolCompanias.get(compania,x).get(i));
					//				lista.add(arbolCompanias.get(compania,x).get(i));
					//
				}

			}
		}


		for(Taxi taxito: definitiva)
		{
			System.out.println(taxito.getServiciosArea().get(zonaInicio).size()+" servicios "+"Taxi: "+taxito.getTaxiId());

			for(int g=0;g<taxito.getServiciosArea().get(zonaInicio).size();g++)
			{
				System.out.println(temp.getServiciosArea().get(zonaInicio).get(g).getStartTime());

			}
		}
		System.out.println("___________________________________");;



		//		System.out.println(temp.getServiciosArea().get(zonaInicio).size());
		//		
		//		for(int j=0;j<arbolCompanias.get(compania,x).get().getServiciosArea().get(zonaInicio).size();i++)
		//		{
		//			if(arbolCompanias.get(compania,x).get(i).getServiciosArea().get(zonaInicio).size()==temp.getServiciosArea().get(zonaInicio).size())
		//			{
		//				
		//				
		//			}
		//		}

		//		System.out.println(temp.getServiciosArea().size());



		return lista;
	}

	//-----------------------------------------
	// Metodos parte B
	//-----------------------------------------
	
	//B1
	public List<Servicio> B1ServiciosPorDistancia(double distanciaMinima, double distanciaMaxima)
	{
		Comparator<Double> c=new MilesComparator();
		List<Servicio> lista= new List<Servicio>();
		NodeTree<Double,List<Servicio>> nod=arbolServicios.getRoot();
	
		listing(nod, lista, distanciaMinima, distanciaMaxima);
	
	
	
		return lista;
	}



	public List<Servicio> B2ServiciosPorZonaRecogidaYLlegada(int zonaInicio, int zonaFinal, String fechaI, String fechaF, String horaI, String horaF) throws ParseException, Exception
	{
		List<Servicio> lista = new List<Servicio>();

		SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
		String tupla= zonaInicio+"-"+zonaFinal;
		String fechaIniS=(fechaI+'T'+horaI+":00.000");
		String FechaFS=(fechaF+'T'+horaF+":00.000");

		Date fechaInicial= formato.parse(fechaIniS);
		Date fechaFinal= formato.parse(FechaFS);
		RangoFechaHora rango= new RangoFechaHora(fechaInicial, fechaFinal);
		RedBlackTree<Date, List<Servicio>> arbol= serviciosZonas.get(tupla);
		if(arbol!=null)
		{
			listingDate(arbol.getRoot(), lista, fechaInicial, fechaFinal);
		}
		else 
		{
			throw new Exception("El arbol esta vacio");
		}

		return lista;
	}


	@Override
	public int[] servicesInInverseOrder() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int[] servicesInOrder() {
		// TODO Auto-generated method stub
		return null;
	}









	public List<Servicio> getServices(int area)
	{
		List<Servicio> temp = serviciosArea.get(area);



		return temp.sort(new Comparator<Servicio>() {

			@Override
			public int compare(Servicio o1, Servicio o2) {
				try {
					return o1.getDropoff_start_timestamp().compareTo((o2.getDropoff_start_timestamp()));
				} catch (Exception e) {
					// TODO: handle exception

					return -1;
				}

			}			
		});

	}


	public void sortCompania(Compania arr[])
	{
		int n = arr.length;

		// Construye inicialmente el �rbol.

		for (int i = n / 2 ; i > 0; i--)
			heapifyI(arr, n, i);

		// Extrae los elementos del �rbol.
		for (int i=n-1; i>1; i--)
		{
			// Mueve la ra�z actual al final.
			Compania temp = arr[1];
			arr[1] = arr[i];
			arr[i] = temp;

			// Aplica recursividad.
			heapifyI(arr, i, 1);
		}


	}
	void heapifyI(Compania arr[], int n, int i)
	{
		int largest = i;  // Inicializa el mayor como la ra�z.
		int l = 2*i;  // left = 2*i
		int r = 2*i + 1;  // right = 2*i + 1

		// Si el hijo de la izquierda es mayor a la ra�z.
		if (l < n)
		{
			if(arr[l].getNumServicios()-arr[largest].getNumServicios()<0)
				largest = l;	
		}

		// Si el hijo de la derecha es mayor a la ra�z.
		if (r < n && arr[r].getNumServicios()-arr[largest].getNumServicios()<0)
			largest = r;

		// Si el mayor no es la ra�z
		if (largest != i)
		{
			Compania swap = arr[i];
			arr[i] = arr[largest];
			arr[largest] = swap;

			// Aplica recursividad.
			heapifyI(arr, n, largest);
		}
	}

	@Override
	public Compania[] servicesPriority() {
		// TODO Auto-generated method stub
		return null;
	}

	public String MinuteRounder(int num)
	{
		if (num<8)
		{
			return "00";
		}
		else if (num<22)
		{
			return "15";
		}
		else if (num<37)
		{
			return "30";
		}
		else if (num<52)
		{
			return "45";
		}
		else 
		{
			return "/";
		}
	}

	public Comparator<Servicio> milesRounder()
	{
		return new MilesRounder();
	}
	private class MilesRounder implements Comparator<Servicio>
	{

		@Override
		public int compare(Servicio o1, Servicio o2) {
			return (((int) o1.getTripMiles()== (int)o2.getTripMiles()))?0:1;

		}
	}

	private class MilesComparator implements Comparator<Double>
	{

		@Override
		public int compare(Double o1, Double o2) {
			if( o1-o2<0.0){
				//System.out.println(o1+ " < "+ o2);
				return -1;
			}
			else if (o1-o2>0.0){
				//System.out.println(o1+ " > "+ o2);
				return 1;
			}
			else{ 
				//System.out.println(o1+ " = "+ o2);
				return 0;
			}
		}

	}

	public void sortPuntos(Taxi arr[])
	{
		int n = arr.length;

		// Construye inicialmente el �rbol.

		for (int i = n / 2 ; i > 0; i--)
			heapifyP(arr, n, i);

		// Extrae los elementos del �rbol.
		for (int i=n-1; i>1; i--)
		{
			// Mueve la ra�z actual al final.
			Taxi temp = arr[1];
			arr[1] = arr[i];
			arr[i] = temp;

			// Aplica recursividad.
			heapifyP(arr, i, 1);
		}


	}
	void heapifyP(Taxi arr[], int n, int i)
	{
		int largest = i;  // Inicializa el mayor como la ra�z.
		int l = 2*i;  // left = 2*i
		int r = 2*i + 1;  // right = 2*i + 1

		// Si el hijo de la izquierda es mayor a la ra�z.
		if (l < n)
		{
			if(arr[l].darPuntos()-arr[largest].darPuntos()<0)
				largest = l;	
		}

		// Si el hijo de la derecha es mayor a la ra�z.
		if (r < n && arr[r].darPuntos()-arr[largest].darPuntos()<0)
			largest = r;

		// Si el mayor no es la ra�z
		if (largest != i)
		{
			Taxi swap = arr[i];
			arr[i] = arr[largest];
			arr[largest] = swap;

			// Aplica recursividad.
			heapifyP(arr, n, largest);
		}
	}


	private class ListComparator implements Comparator<List>
	{

		@Override
		public int compare(List o1, List o2) {
			// TODO Auto-generated method stub
			return o1.compareTo(o2);
		}

	}



	private class NameComparator implements Comparator<String>
	{

		@Override
		public int compare(String o1, String o2) {
			// TODO Auto-generated method stub
			return o1.compareTo(o2);
		}

	}


	private class DateComparator implements Comparator<Date>
	{

		@Override
		public int compare(Date o1, Date o2) {
			// TODO Auto-generated method stub
			return o1.compareTo(o2);
		}

	}

	@Override
	public List<Servicio> servicesMiles(double pMillas) {
		// TODO Auto-generated method stub
		return serviciosMillasRound.get(pMillas);
	}

	public void hours(NodeTree<Date,List<Servicio>> x, List<Servicio> lista, Date lo)
	{
		if (x == null) return ; 
		int cmp = lo.compareTo(x.getKey()); 
		
		if (cmp < 0)  
			hours(x.getLeft(), lista, lo); 
		if (cmp == 0)
		{
			List<Servicio> l= x.getValue();
			for(Servicio ser: l)
			{
				if(ser.getPickup_community_area()!=ser.getDropoff_community_area())
				{
					lista.add(ser);
				}
			}
		}

		if (cmp > 0)
			hours(x.getRight(), lista, lo);
		return;
	}

	public void listing(NodeTree<Double,List<Servicio>> x, List<Servicio> lista, Double lo, Double hi)
	{
		if (x == null) return ; 
		int cmplo = lo.compareTo(x.getKey()); 
		int cmphi = hi.compareTo(x.getKey()); 
		if (cmplo < 0)  
			listing(x.getLeft(), lista, lo, hi); 
		if (cmplo <= 0 && cmphi >= 0)
		{
			List<Servicio> l= x.getValue();
			for(Servicio ser: l)
			{
				lista.add(ser);
			}
		}

		if (cmphi > 0) listing(x.getRight(), lista, lo, hi);
		return;
	}
	
	public void listingDate(NodeTree<Date,List<Servicio>> x, List<Servicio> lista, Date lo, Date hi)
	{
		if (x == null) return ; 
		int cmplo = lo.compareTo(x.getKey()); 
		int cmphi = hi.compareTo(x.getKey()); 
		if (cmplo < 0)  
			listingDate(x.getLeft(), lista, lo, hi); 
		if (cmplo <= 0 && cmphi >= 0)
		{
			List<Servicio> l= x.getValue();
			for(Servicio ser: l)
			{
				lista.add(ser);
			}
		}

		if (cmphi > 0) listingDate(x.getRight(), lista, lo, hi);
		return;
	}




	@Override
	public List<Servicio> A2ServiciosPorDuracion(int duracion) {
		// TODO Auto-generated method stub

		RangoDuracion temporal = new RangoDuracion(0, 0);
		RangoDuracion uno = listaRangos.get(0);


		for(int i=0; i<listaRangos.size();i++)
		{
			if(duracion>=listaRangos.get(i).getLimineInferior()&&duracion<=listaRangos.get(i).getLimiteSuperior())
			{
				temporal = listaRangos.get(i);
				//				System.out.println("Cre� una consulta");
			}
		}

		System.out.println(temporal.getLimineInferior());
		System.out.println(temporal.getLimiteSuperior());

		//		for(int i=0; i<serviciosRango.size();i++)
		//		{
		//			if(serviciosRango.get(listaRangos.get(i)).get(0)!=null)
		//			{
		//				System.out.println(serviciosRango.get(listaRangos.get(i)).get(0).getTripSeconds());
		//			}
		//			
		//		}
		//		

		//		System.out.println(serviciosRango.get(uno));
		System.out.println(serviciosRango.get(uno).get(0).getTripSeconds());

		//		retorno = serviciosRango.get(uno);
		//		System.out.println(retorno.size());
		System.out.println(serviciosRango.size());



		return (List<Servicio>) temporal.getServiciosEnRango();
	}


	@Override
	public Taxi[] R1C_OrdenarTaxisPorPuntos() {
		// TODO Auto-generated method stub
		Taxi[] retorno = new Taxi[colaTaxis.size()];

		for(int i=0; i<colaTaxis.size();i++)
		{
			retorno[i]= colaTaxis.get(i);
		}
		sortPuntos(retorno);
		return retorno;
	}






	@Override
	public List<Servicio> R2C_LocalizacionesGeograficas(String taxiIDReq2C, double millas) {
		// TODO Auto-generated method stub

		List<Servicio> lista= new List<Servicio>();
	
		
		
		
	
		

		return lista;

	}


	@Override
	public List<Servicio> R3C_ServiciosEn15Minutos(String fecha, String hora) {
		SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
		Date fech;
		String f[]=fecha.split("-");
		String h[]=hora.split(":");
		int min= Integer.parseInt(h[1]);
		int fechaFinal=0;
		int horaFinal=0;
		int minutosFinal=0;
		String roundedMinutes= MinuteRounder(min);
		if(roundedMinutes.equals("/"))
		{
			if(Integer.parseInt(h[1])==23)
			{
				fechaFinal=Integer.parseInt(f[2])+1;
				horaFinal=0;
				minutosFinal=0;
			}
			else
			{
				horaFinal =Integer.parseInt(h[0])+1;
				minutosFinal=0;
			}

		}
		else
		{
			fechaFinal= Integer.parseInt(f[2]);
			horaFinal=Integer.parseInt(h[0]);
			minutosFinal=Integer.parseInt(roundedMinutes);
		}
		List<Servicio> lista= new List<Servicio>();
		String fechaS=f[0]+
				"-"+f[1]+"-0"+fechaFinal+'T'+horaFinal+":"+minutosFinal;
		System.out.println(f[2] +"dia");
		System.out.println(fechaS);
		try {
			fech= formato.parse(fechaS);
			System.out.println(fech +"fecha parseada" );
			
			hours(arbolHoras.getRoot(), lista, fech);
		} catch (ParseException e) {
			System.out.println("error ");
		}

		
		// TODO Auto-generated method stub
		return lista;
	}
}




// Cambios para el push

/*
 * get pickupZoneServicio
 */






