package api;

import model.vo.Taxi;
import model.vo.TaxiConPuntos;
import model.vo.TaxiConServicios;

import java.text.ParseException;

import model.data_structures.LinkedList;
import model.data_structures.List;
import model.vo.Compania;
import model.vo.InfoTaxiRango;
import model.vo.RangoFechaHora;
import model.vo.Servicio;

/**
 * Basic API for testing the functionality of the TaxiTrip manager
 */
public interface ITaxiTripsManager {

	/**
	 * Method to load the services of a specific taxi 
	 * The services are loaded in both a Stack and a Queue 
	 * @param servicesFile - path to the JSON file with taxi services 
	 * @param taxiId - taxiId of interest 
	 */
	public boolean loadServices(String serviceFile);
	
	public List<Servicio> getServices(int pArea);

	

    /**
	 * Method to return the number of services in inverse order by trip_start_timestamp.
	 * The search applies to the services in the Stack
	 * @return array with two integer positions: number of services in inverse order at position 0, and number of services not in inverse order at position 1
	 */
	public int [] servicesInInverseOrder();
	
    /**
	 * Method to return the number of services in order by trip_start_timestamp.
	 * The search applies to the services in the Queue
	 * @return array with two integer positions: number of services in order at position 0, and number of services not in order at position 1
	 */
	public int [] servicesInOrder();


	public Compania[] servicesPriority();

	public List<Servicio> servicesMiles(double pMillas);

	public LinkedList<Servicio> B1ServiciosPorDistancia(double distanciaMinima, double distanciaMaxima);

	public LinkedList<Servicio> B2ServiciosPorZonaRecogidaYLlegada(int zonaInicio, int zonaFinal, String fechaI,
			String fechaF, String horaI, String horaF) throws ParseException, Exception;

	public LinkedList<Taxi> A1TaxiConMasServiciosEnZonaParaCompania(int zonaInicio, String compania);

	public LinkedList<Servicio> A2ServiciosPorDuracion(int duracion);

	public Taxi[] R1C_OrdenarTaxisPorPuntos();

	public LinkedList<Servicio> R2C_LocalizacionesGeograficas(String taxiIDReq2C, double millas);

	public LinkedList<Servicio> R3C_ServiciosEn15Minutos(String fecha, String hora);
	



	
}
