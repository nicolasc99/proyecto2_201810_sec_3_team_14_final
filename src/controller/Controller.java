package controller;

import java.text.ParseException;

import api.ITaxiTripsManager;
import model.data_structures.LinkedList;
import model.logic.TaxiTripsManager;
import model.vo.Servicio;
import model.vo.Taxi;
import model.vo.TaxiConPuntos;
import model.vo.TaxiConServicios;

public class Controller 
{
	/**
	 * modela el manejador de la clase l�gica
	 */
	private static ITaxiTripsManager manager =new TaxiTripsManager();

	//Carga El sistema
	public static boolean cargarSistema(String direccionJson)
	{
		return manager.loadServices(direccionJson);
	}
	//1A
	public static LinkedList<Taxi> R1A(int zonaInicio, String compania)
	{
		return manager.A1TaxiConMasServiciosEnZonaParaCompania(zonaInicio, compania);
	}

	//2A
	public static LinkedList<Servicio> R2A(int duracion)
	{
		return manager.A2ServiciosPorDuracion(duracion);
	}

	//1B
	public static LinkedList<Servicio> R1B(double distanciaMinima, double distanciaMaxima)
	{
		return manager.B1ServiciosPorDistancia(distanciaMinima, distanciaMaxima);
	}

	//2B
	public static LinkedList<Servicio> R2B(int zonaInicio, int zonaFinal, String fechaI, String fechaF, String horaI, String horaF) throws ParseException, Exception
	{
		return manager.B2ServiciosPorZonaRecogidaYLlegada(zonaInicio, zonaFinal, fechaI, fechaF, horaI, horaF);
	}	
	//1C
	public static Taxi[] R1C()
	{
		return manager.R1C_OrdenarTaxisPorPuntos();
	}	
	//2C
	public static LinkedList<Servicio> R2C(String taxiIDReq2C, double longitud)
	{
		return manager.R2C_LocalizacionesGeograficas(taxiIDReq2C, longitud);
	}
	//3C
	public static LinkedList<Servicio> R3C(String fecha, String hora) 
	{
		return manager.R3C_ServiciosEn15Minutos(fecha, hora);
	}	
}
